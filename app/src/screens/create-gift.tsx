import React from 'react';

import { isIosDeviceUsingChrome } from '../utils/helpers';
import { canUseAudioRecorder } from '../utils/use-audio-recorder';

import { t } from '../i18n';
import { museum } from '../data';
import { UnsupportedDevice } from '../components/messages/unsupported-device';
import { CreateGift } from '../components/creating/create-gift';

/**
 * Create gift screen
 */

const CreateGiftScreen: React.FC = () => {

  // Check the device meets our requirements

  // If this is an iOS device using Chrome prompt the user to use Safari, as they will have it
  if (isIosDeviceUsingChrome()) {
    return (
      <UnsupportedDevice museum={museum} message={t('please-try-in-a-different-browser')} />
    );
  }

  // If we can't record audio inform and force end
  if (!canUseAudioRecorder()) {
    return (
      <UnsupportedDevice museum={museum} message={t('phone-cant-record-audio')} />
    );
  }

  // Show
  return <CreateGift museum={museum} />;
};

export {
  CreateGiftScreen,
};
