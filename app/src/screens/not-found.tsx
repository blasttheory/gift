import React from 'react';

import { t } from '../i18n';
import { ErrorMessage } from '../components/messages/error-message';

export const NotFound: React.FC = () => (
  <ErrorMessage message={t('page-not-found')} />
);
