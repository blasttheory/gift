import I18n from 'i18n-js';

import { Locale } from '../domain';

import { translations as enTranslations, Translations } from './langs/en';


export function i18nSetup(): void {
  I18n.defaultLocale = 'en';
  I18n.translations = {
    en: enTranslations,
  };
}

export function setLocale(locale: Locale): void {
  I18n.locale = locale;
};

// A budget version of I18n.t which enforces type-safety against our translation
// files. This should be used to ensure that all translations exist.
export function t(key: keyof Translations, options?: I18n.TranslateOptions): string {
  return I18n.t(key, options);
}
