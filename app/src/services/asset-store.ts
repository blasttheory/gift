/**
 * An AssetStore is responsible for holding urls for all assets needed
 * throughout the app. It provides an interface to preload all the assets --
 * converting then to browser ObjectUrls.
 *
 * TODO: Expose interface via state and event subscription rather than awaiting promise.
 * TODO: Provide progress updates via subscription
 * TODO: Retry / error handling etc
 */
export class AssetStore<AssetKeys extends string> {

  /**
   * Asset URLs should be retrieved from here.
   *
   * As assets are preloaded, the urls in here will be altered to browser local
   * ObjectUrls.
   */
  public assets: Record<AssetKeys, string>;

  // For now we're not clever enough to run more than once and handle failures,
  // so we use this to ensure preload() is only called once.
  private preloadStarted = false;

  /**
   * Create a new asset-store for the given collection of { key: url } assets.
   */
  public constructor(assets: Record<AssetKeys, string>) {
    this.assets = { ...assets };
  }

  /**
   * Preload all the assets in the store and convert to browser local ObjectUrls.
   */
  public preload = async () => {
    // Prevent running more than once
    if (this.preloadStarted) return;
    this.preloadStarted = true;

    const promises = Object.entries(this.assets).map(([key, url]) => {
      return new Promise<void>((resolve, reject) => {
        const req = new XMLHttpRequest();
        req.open('GET', url as string, true);
        req.responseType = 'blob';

        req.onload = () => {
          try {
            if (req.status !== 200) throw new Error();
            const objectUrl = URL.createObjectURL(req.response);
            (this.assets as any)[key] = objectUrl;
            resolve();
          } catch (err) {
            reject(err);
          }
        };

        req.onerror = reject;
        req.send();
      });
    });

    await Promise.all(promises);
  }
}
