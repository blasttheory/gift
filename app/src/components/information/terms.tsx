import React, { useEffect } from 'react';

import { events } from '../../services';
import { pTermsPrivacyOpenedEvent, pTermsPrivacyClosedEvent } from '../../event-definitions';

import { Markdown } from '../markdown';


export const TermsContent: React.FC<{
  markdown: string;
}> = (p) => {
  useEffect(() => {
    events.track(pTermsPrivacyOpenedEvent());
    return () => events.track(pTermsPrivacyClosedEvent());
  }, []);

  return <Markdown content={p.markdown} allowDangerousHtml={true} />
};
