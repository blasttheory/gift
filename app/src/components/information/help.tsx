import React, { useEffect } from 'react';

import { events } from '../../services';
import { pHelpOpenedEvent, pHelpClosedEvent } from '../../event-definitions';

import { Markdown } from '../markdown';


export const HelpContent: React.FC<{
  markdown: string;
}> = (p) => {
  useEffect(() => {
    events.track(pHelpOpenedEvent());
    return () => events.track(pHelpClosedEvent());
  }, []);

  return <Markdown content={p.markdown} allowDangerousHtml={true} />;
};
