import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

import { t } from '../../i18n';
import { assertNever } from '../../utils/helpers';
import { InProgressGift, Gift } from '../../domain';
import { useGiftSaver, events } from '../../services';

import {
  cSavingAttemptedEvent,
  cSavingSucceededEvent,
  cSavingFailedEvent,
  cSavingRetriedEvent,
} from '../../event-definitions';

import { Panel, PanelContent } from '../panel';
import { PanelTitle } from '../panel-title';
import { PanelSubTitle } from '../panel-sub-title';
import { PanelPrompt } from '../panel-prompt';
import { PanelRound } from '../panel-round';
import { PanelButtons } from '../panel-buttons';
import { Button } from '../buttons';
import { ProgressLoader } from '../progress-loader';


interface Props {
  gift: InProgressGift;
  onComplete: (gift: Gift) => void;
}

export const SaveGift: React.FC<Props> = ({ gift, onComplete }) => {
  const saver = useGiftSaver(gift);

  useEffect(() => { events.track(cSavingAttemptedEvent(gift.id)); }, []);

  // Actions on saver state-transitions
  useEffect(() => {
    if (saver.kind === 'done') {
      events.track(cSavingSucceededEvent(gift.id));
      onComplete(saver.gift);
    }
    if (saver.kind === 'invalid-gift') {
      events.track(cSavingFailedEvent(gift.id, saver.kind));
    }
    if (saver.kind === 'uploading-assets-error') {
      events.track(cSavingFailedEvent(gift.id, saver.kind));
    }
    if (saver.kind === 'saving-gift-error') {
      events.track(cSavingFailedEvent(gift.id, saver.error.kind));
    }
  }, [saver.kind]);

  // Cleanup on exit
  useEffect(() => () => saver.abort(), []);


  if (saver.kind === 'uploading-assets') {
    return <SavingInProgress text={t('saving-your-gift')} progress={Math.round(saver.progress * 100)} />;
  }

  if (saver.kind === 'saving-gift' || saver.kind === 'done') {
    return <SavingInProgress text={t('processing-gift-please-be-patient')} />;
  }

  if (saver.kind === 'invalid-gift') {
    return (
      <SavingFailedUnrecoverable
        text={t('problem-saving-gift-cant-be-recovered')}
      />
    );
  }

  if (saver.kind === 'uploading-assets-error') {
    return (
      <SavingFailed
        text={t('please-check-youre-connected-to-the-internet')}
        buttonText={t('try-again')}
        onClick={() => {
          events.track(cSavingRetriedEvent(gift.id));
          saver.retry();
        }}
      />
    );
  }

  if (saver.kind === 'saving-gift-error') {
    if (saver.error.kind === 'http-error') {
      return (
        <SavingFailed
          text={t('problem-saving-gift-try-again')}
          buttonText={t('try-again')}
          onClick={() => {
            events.track(cSavingRetriedEvent(gift.id));
            saver.retry();
          }}
        />
      );
    }

    return (
      <SavingFailed
        text={t('please-check-youre-connected-to-the-internet')}
        buttonText={t('try-again')}
        onClick={() => {
          events.track(cSavingRetriedEvent(gift.id));
          saver.retry();
        }}
      />
    );
  }

  return assertNever(saver);
};



interface SavingInProgressProps {
  text: string;
  progress?: number;
}
export const SavingInProgress: React.FC<SavingInProgressProps> = ({ text, progress }) => (
  <Panel>
    <PanelTitle>{t('finish-your-gift')}</PanelTitle>
    <PanelSubTitle>{t('saving-it')}</PanelSubTitle>
    <PanelContent>
      <PanelRound background='transparent-black'>
        <ProgressLoader text={text} percent={progress} colourTheme='white' />
      </PanelRound>
    </PanelContent>
  </Panel>
);


interface SavingFailedProps {
  text: string;
  buttonText: string;
  onClick: () => void;
}
const SavingFailed: React.FC<SavingFailedProps> = ({ text, buttonText, onClick }) => (
  <Panel>
    <PanelTitle>{t('finish-your-gift')}</PanelTitle>
    <PanelSubTitle>{t('saving-failed')}</PanelSubTitle>
    <PanelContent>
      <PanelPrompt background='transparent-black' text={text} />
    </PanelContent>
    <PanelButtons>
      <Button onClick={onClick} primary={true}>{buttonText}</Button>
    </PanelButtons>
  </Panel>
);



interface SavingFailedUnrecoverableProps {
  text: string;
}
const SavingFailedUnrecoverable: React.FC<SavingFailedUnrecoverableProps> = ({ text }) => {
  const navigate = useNavigate();

  return (
    <Panel>
      <PanelTitle>{t('finish-your-gift')}</PanelTitle>
      <PanelSubTitle>{t('saving-failed')}</PanelSubTitle>
      <PanelContent>
        <PanelPrompt background='transparent-black' text={text} />
      </PanelContent>
      <PanelButtons>
        <Button onClick={() => navigate('/')} primary={true}>{t('home')}</Button>
      </PanelButtons>
    </Panel>
  );
}
