import React, { useState } from 'react';

import { museum } from '../../data';
import { t } from '../../i18n';
import { assetStore } from '../../services';

import { Panel, PanelContent } from '../panel';
import { PanelButtons } from '../panel-buttons';
import { Button } from '../buttons';
import { AudioPlayer } from '../media/audio-player';
import { AudioTranscription } from '../media/audio-transcription';
import { TextInputModal } from '../modals/text-input-modal';

/**
 * The start of making a gift. User enters recipient name.
 */

interface Props {
  giftId: string;
  onComplete: (recipientName: string) => void; // Callback to call when name is entered
}

export const CreateGiftChooseRecipient: React.FC<Props> = ({ giftId, onComplete }) => {

  // State
  const [showingEnterRecipient, setShowingEnterRecipient] = useState(false);
  const [audioHasPlayed, setAudioHasPlayed] = useState(false);

  return (
    <>

      {showingEnterRecipient && (
        <TextInputModal
          placeHolder={t('their-first-name')}
          onSaveClick={(recipientName) => { onComplete(recipientName); }}
          onCancelClick={() => { setShowingEnterRecipient(false); }}
        />
      )}

      <Panel>

        <AudioTranscription
          giftId={giftId}
          audioReference={'c-choose-recipient'}
          transcript={museum.audio.cStart.transcript}
        />

        <PanelContent>

          <AudioPlayer
            message={t('who-are-you-going-to-choose')}
            src={assetStore.assets.cStart}
            giftId={giftId}
            audioReference={'c-choose-recipient'}
            onPlaybackComplete={() => setAudioHasPlayed(true)}
          />

        </PanelContent>

        <PanelButtons>
          {audioHasPlayed && (
            <Button onClick={() => { setShowingEnterRecipient(true); }} primary={true}>
              {t('enter-their-name')}
            </Button>
          )}
        </PanelButtons>

      </Panel>

    </>
  );
};
