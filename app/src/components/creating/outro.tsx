import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

import { t } from '../../i18n';
import { museum } from '../../data';
import { assetStore } from '../../services';

import { Panel, PanelContent } from '../panel';
import { PanelButtons } from '../panel-buttons';
import { Button } from '../buttons';
import { FeedbackModal } from '../modals/feedback-modal';
import { AudioPlayer } from '../media/audio-player';
import { AudioTranscription } from '../media/audio-transcription';
import { InProgressGift } from '../../domain';

import { events } from '../../services';
import {
  cOutroCompletedEvent,
} from '../../event-definitions';


/**
 * Show the creating outro
 */

export interface Props {
  gift: InProgressGift;
}

const CreatingOutro: React.FC<Props> = ({ gift }) => {
  const navigate = useNavigate();

  // State
  const [audioPlaybackFinished, setAudioPlaybackFinished] = useState(false);
  const [showFeedbackModal, setShowFeedbackModal] = useState(false);


  function handleContinue() {

    events.track(cOutroCompletedEvent(gift.id));

    // Go to the home screen
    navigate('/');
  }


  return (
    <Panel>

      {showFeedbackModal &&
       <FeedbackModal feedbackUrl={museum.feedbackUrl} feedbackText={museum.feedbackText} onFinished={() => {setShowFeedbackModal(false); }} />
      }

      <AudioTranscription
        giftId={gift.id}
        audioReference={'c-outro'}
        transcript={museum.audio.cShare.transcript}
      />

      <PanelContent>
        <AudioPlayer
          message={t('thank-you')}
          src={assetStore.assets.cShare}
          giftId={gift.id}
          audioReference={'c-outro'}
          onPlaybackComplete={() => {
            setShowFeedbackModal(true);
            setAudioPlaybackFinished(true);
          }}
        />
      </PanelContent>

      <PanelButtons>
        {audioPlaybackFinished && <Button primary={true} onClick={handleContinue}>{t('done')}</Button>}
      </PanelButtons>

    </Panel>
  );

};

export {
  CreatingOutro,
};
