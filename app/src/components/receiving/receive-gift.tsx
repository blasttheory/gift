import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import styled from 'styled-components';

import { t } from '../../i18n';

import { events } from '../../services';
import { rAtMuseumConfirmedEvent, rOpenPressedEvent } from '../../event-definitions';

import { Gift, Museum } from '../../domain';
import { GlobalStyles, global } from '../../themes/global';
import { ScreenManager } from '../screen-manager';
import { TextResize } from '../text-resize';
import { ScreenHeader } from '../screen-header';
import { GiftPartsManager } from './gift-parts-manager';
import { ChooseLocation, RecipientLocation } from '../choose-location';
import { Button } from '../buttons';
import { ReceivingOpenGift } from './open-gift';
import { Panel, PanelContent } from '../panel';
import { PanelButtons } from '../panel-buttons';
import { PanelPrompt } from '../panel-prompt';
import { BackgroundSvg } from '../background-svg';
import {
  getSessionRecipientLocation,
  setSessionRecipientLocation,
} from '../../utils/local';
import { assertNever } from '../../utils/helpers';
import { museum } from '../../data';

/**
 * Gift Receive screen top level component
 */

export const MainTitle = styled(TextResize).attrs({
  textSize: 40,
})`
  z-index: 1;
  text-align: center;
  font-family: ${global.fonts.title.family};
  font-weight: ${global.fonts.title.bold};
  margin: 5vh 0 0;
`;

export const MuseumName = styled(TextResize).attrs({
  textSize: 150,
})`
  z-index: 1;
  width: 90%;
  text-align: center;
  font-family: ${global.fonts.title.family};
  font-weight: ${global.fonts.title.bold};
  line-height: 0.9;
`;

// Current status of this screen
type ReceiveGiftStatus = 'Welcome' | 'SelectLocation' | 'OpenOrSave' | 'ShowingParts';

interface Props {
  gift: Gift;
  museum: Museum;
}


const ReceiveGift: React.FC<Props> = (props) => {

  const [status, setStatus] = useState<ReceiveGiftStatus>('Welcome');
  const [recipientLocation, setRecipientLocation] = useState<RecipientLocation>(getSessionRecipientLocation);
  const [compactHeader, setCompactHeader] = useState<boolean>(false);

  const navigate = useNavigate();


  const handleOpenClicked = () => {
    events.track(rOpenPressedEvent(props.gift.id));

    if (recipientLocation === 'unknown') {
      setStatus('SelectLocation');
    } else {
      setStatus('ShowingParts');
    }
  }


  const handleOpenAnywayClicked = () => {
    setCompactHeader(true);
    setStatus('ShowingParts');
  }


  const handleSetLocation = (location: RecipientLocation) => {
    if (location === 'at-museum') events.track(rAtMuseumConfirmedEvent(props.gift.id, true));
    if (location === 'not-at-museum') events.track(rAtMuseumConfirmedEvent(props.gift.id, false));

    setSessionRecipientLocation(location);

    // Determine the next stage based on the location
    const nextStage: ReceiveGiftStatus = location === 'not-at-museum'
      ? 'OpenOrSave'
      : 'ShowingParts';

    // Store this
    setRecipientLocation(location);
    setStatus(nextStage);
  }

  // The header size is based on our current state
  const headerSize = compactHeader ? 'compact'
      : status === 'Welcome' || status === 'SelectLocation' || status === 'OpenOrSave' ? 'big'
      : 'small';

  const giftFromLabel = (props.gift.kind === 'MuseumGift') ? museum.name
                      : (props.gift.kind === 'PersonalGift') ? t('sender-at-museum', {
                        senderName: props.gift.senderName,
                        museumName: museum.name,
                      })
                      : assertNever(props.gift.kind);

  return (
    <ScreenManager>
      <BackgroundSvg />
      <GlobalStyles />

      {headerSize === 'big' && (
        <>
          <ScreenHeader
            showLogo={false}
            museum={props.museum}
          />
          <MainTitle>{t('heres-your-gift-from')}</MainTitle>
          <MuseumName>{giftFromLabel}</MuseumName>
        </>
      )}
      {headerSize === 'small' && (
        <ScreenHeader
          postSubTitle={t('your-gift-from')}
          title={giftFromLabel}
          showLogo={false}
          background='white'
          museum={props.museum}
        />
      )}
    {headerSize === 'compact' && (
      <ScreenHeader
        postSubTitle={t('your-gift-from')}
        title={giftFromLabel}
        showLogo={false}
        background='white'
        museum={props.museum}
      />
    )}

    {status === 'Welcome' && (
      <ReceivingOpenGift onComplete={handleOpenClicked} />
    )}

      {status === 'OpenOrSave' && (
        <Panel>
          <PanelContent>
            <PanelPrompt
              text={t('open-it-now-or-save-your-gift-for-when-youre-at-the-museum')}
              background={'transparent-black'}
            />
          </PanelContent>
          <PanelButtons>
            <Button onClick={handleOpenAnywayClicked} primary={true}>{t('open-it-now')}</Button>
            <Button onClick={() => navigate('/')}>{t('save-it')}</Button>
          </PanelButtons>
        </Panel>
      )}

      {status === 'SelectLocation' && (
        <ChooseLocation
          museumName={props.museum.name}
          onLocationSelected={handleSetLocation}
        />
      )}

      {status === 'ShowingParts' && (
        <GiftPartsManager
          gift={props.gift}
          recipientLocation={recipientLocation}
        />
      )}
    </ScreenManager>
  );
}

export {
  ReceiveGift,
};
