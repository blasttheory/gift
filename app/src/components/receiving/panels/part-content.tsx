import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import styled from 'styled-components';

import { t } from '../../../i18n';

import { museum } from '../../../data';
import { assetStore } from '../../../services';
import { events } from '../../../services';
import {
  rPartCluePressedEvent,
  rPartClueDismissedEvent,
  rPartHelpPressedEvent,
  rPartHelpDismissedEvent,
  rPartCompletedEvent,
  rPartFound,
  rOutroCompletedEvent,
} from '../../../event-definitions';

import { Panel, PanelContent } from '../../panel';
import { PanelPrompt } from '../../panel-prompt';
import { PanelText } from '../../panel-text';
import { PanelButtons } from '../../panel-buttons';
import { Button } from '../../buttons';
import { FeedbackModal } from '../../modals/feedback-modal';
import { AudioPlayer } from '../../media/audio-player';
import { AudioTranscription } from '../../media/audio-transcription';
import { RecipientLocation } from '../../choose-location';
import { Gift, GiftPart } from '../../../domain';
import { WaitThen, WaitThenShow } from '../../utils/wait-then';
import SvgIconDone from '../../svg/icon-done';

/**
 * Show the gift part content, prompting for clues, etc.
 */

const DoneIconWrap = styled.div`
  width: 20%;
  position: relative;
`;

export interface PartContentProps {
  gift: Gift; // The gift in question, as we need some other info (part count, sender name)
  giftPartIndex: number; // The index of this gift part
  recipientLocation: RecipientLocation; // At the museum or not
  onComplete?: () => void; // Callback to call when complete
  revealPreviewImage: () => void; // Callback reveal the preview circle
  revealBackgroundImage: () => void; // Callback reveal the entire background image
}

type Section =
  | 'start'
  | 'reveal-preview2'
  | 'show-clue-search'
  | 'need-help'
  | 'help-is-here'
  | 'reveal-full'
  | 'play-audio'
  | 'show-clue-found'
  | 'unwrapped'
  | 'outro'
;

const ReceivingPartContent: React.FC<PartContentProps> = (props) => {
  const navigate = useNavigate();

  const [section, setSection] = useState<Section>('start');
  const [audioPlaybackComplete, setAudioPlaybackComplete] = useState(false);
  const [outroAudioPlaybackFinished, setOutroAudioPlaybackFinished] = useState(false);
  const [showFeedbackModal, setShowFeedbackModal] = useState(false);

  // Get some local references
  const giftPart: GiftPart = props.gift.parts[props.giftPartIndex];
  const giftPartCount: number = props.gift.parts.length;
  const senderName: string = props.gift.senderName;
  const atMuseum = (props.recipientLocation === 'at-museum');
  const museumGift = (props.gift.kind === 'MuseumGift');

  // Our audio player has finished
  function handleAudioPlaybackFinished() {
    setAudioPlaybackComplete(true);
  }

  // Show the preview image circle
  function revealPreviewImage() {

    // Fire the call back
    if (props.revealPreviewImage) {
      props.revealPreviewImage();
    }

    // Set our section
    setSection('reveal-preview2');
  }

  function gotoSneakPeek() {
    setSection('start');
  }

  function gotoGiveClueSearch() {

    // Record the event
    events.track(rPartCluePressedEvent(props.gift.id, props.giftPartIndex + 1));

    // Show the section
    setSection('show-clue-search');
  }

  function gotoGiveHelp() {
    setSection('need-help');
  }

  function gotoHereYouGo() {

    // Record the event
    events.track(rPartFound(props.gift.id, props.giftPartIndex + 1));

    // Show the section
    setSection('help-is-here');
  }

  function gotoFound() {
    if (props.revealBackgroundImage) {
      props.revealBackgroundImage();
    }
    setSection('reveal-full');
  }

  function gotoFoundAudio() {
    setSection('play-audio');
  }

  function gotoOutro() {
    setSection('unwrapped');
  }

  function gotoEndOfGiftPart() {

    events.track(rPartCompletedEvent(props.gift.id, props.giftPartIndex + 1));

    // If on the last part show the outro
    const lastGiftPart = (props.giftPartIndex + 1 === giftPartCount);
    if (lastGiftPart) {
      gotoOutro();
    } else {

      // Callback
      if (props.onComplete) {
        props.onComplete();
      }

    }

  }

  function handleOutroContinue() {

    // Track go home event
    events.track(rOutroCompletedEvent(props.gift.id));

    // Go to the home screen
    navigate('/');
  }


  // Advanced to the next status/screen
  function handleContinue() {

    if (section === 'start') {
      if (atMuseum) {
        revealPreviewImage();
      } else {
        // If not at the museum jump straight to the reveal
        if (props.revealBackgroundImage) {
          props.revealBackgroundImage();
        }
        setSection('reveal-full');
      }
    }

    if (section === 'reveal-preview2') {

      // Check if we have a clue
      if (giftPart && giftPart.clue.trim()) {
        setSection('need-help');
      } else {
        setSection('show-clue-search');
      }
    }

    if (section === 'show-clue-search') { setSection('need-help'); }
    if (section === 'need-help') { setSection('help-is-here'); }
    if (section === 'help-is-here') { setSection('reveal-full'); }
    if (section === 'reveal-full') { setSection('play-audio'); }
    if (section === 'play-audio') { setSection('show-clue-found'); }
    if (section === 'show-clue-found') { setSection('unwrapped'); }
    if (section === 'unwrapped') { setSection('outro'); }

  }


  function getButtons() {

    // Is there a part after this one
    const furtherPart = (giftPartCount > (props.giftPartIndex + 1));

    // Check if we have a clue
    const haveClue = giftPart && giftPart.clue.trim();

    switch (section) {
      case 'reveal-preview2':
        return (
          <>
            {haveClue && <Button onClick={gotoGiveClueSearch}>{t('show-clue')}</Button>}
            {!haveClue && <Button onClick={gotoGiveHelp}>{t('help')}</Button>}
            <Button onClick={gotoHereYouGo} primary={true}>{t('found-it')}</Button>
          </>
        );

      case 'show-clue-search':
        return (
          <>
            <Button onClick={gotoSneakPeek} primary={true}>{t('ok')}</Button>
            <Button
              onClick={() => {
                events.track(rPartHelpPressedEvent(props.gift.id, props.giftPartIndex + 1));
                gotoGiveHelp();
              }}
            >
              {t('more-help')}
            </Button>
          </>
        );
      case 'need-help': // More help
        return (
          <Button
            onClick={() => {
              events.track(rPartHelpDismissedEvent(props.gift.id, props.giftPartIndex + 1));
              gotoSneakPeek();
            }}
            primary={true}
          >
            {t('ok')}
          </Button>
        );
      case 'reveal-full':

        // If at the museum, auto continue, therefore no buttons
        if (props.recipientLocation === 'at-museum') return null;

        // If not at the museum show the buttons
        return (
          <WaitThenShow
            wait={1}
          >
            <Button onClick={handleContinue} primary={true}>{t('continue')}</Button>
          </WaitThenShow>
        );
      case 'play-audio':

        // Different text based on gift part
        // Note: This is never shown on the last part, so no need to consider that case
        const openPartText = (props.giftPartIndex === 1) ? t('open-last-part') : t('open-part-2');

        return (
          <>
            {audioPlaybackComplete && furtherPart && (
              <Button onClick={gotoEndOfGiftPart} primary={true}>{openPartText}</Button>
            )}
            {audioPlaybackComplete && !furtherPart && (
              <Button onClick={gotoEndOfGiftPart} primary={true}>{t('done')}</Button>
            )}
          </>
        );
      case 'show-clue-found':
        return (
          <Button
            onClick={() => {
              events.track(rPartClueDismissedEvent(props.gift.id, props.giftPartIndex + 1));
              gotoFoundAudio();
            }}
            primary={true}
          >
            {t('ok')}
          </Button>
        );
      case 'outro':
        return (
          <>
            {outroAudioPlaybackFinished && <Button primary={true} onClick={handleOutroContinue}>{t('done')}</Button>}
          </>
        );
      default :
        return (
          null
        );
    }
  }

  function getIntroText() {

    // Show different text if at museum

    if (atMuseum) {
      switch (props.giftPartIndex) {
        case 0 :
          // Text changes based on gift count
          return giftPartCount === 1
               ? t('this-is-a-sneek-peek-of-your-gift')
               : `${t('this-is-a-sneek-peek-of-your-gift')} ${t('do-you-know-where-to-look')}`;
        case 1 :
          return `${t('heres-a-look-at-the-second-object')} ${t('take-a-wander-to-find-it')}`;
        case 2 :
          return `${t('heres-a-glimpse-of-your-last-object')} ${t('time-to-see-if-you-can-track-it-down')}`;
        default :
          return '';
      }
    } else  {

      // Not at museum

      return (giftPartCount === 1) ? t('heres-the-object-that-sender-chose', { senderName })
           : (props.giftPartIndex === 0) ? t('heres-the-first-object-that-sender-chose', { senderName })
           : (props.giftPartIndex === 1) ? t('heres-the-second-object-that-sender-chose', { senderName })
           : (props.giftPartIndex === 2) ? t('heres-the-final-object-that-sender-chose', { senderName })
           : '';
    }

  }

  function getNeedHelpText() {
    switch (props.giftPartIndex) {
      case 0 :
        return t('stuck-try-asking-someone');
      case 1 :
        return t('oh-dear-find-someone');
      case 2 :
        return t('the-last-one-ask-someone-in-the-museum');
      default :
        return '';
    }
  }

  function getPreFindText() {
    switch (props.giftPartIndex) {
      case 0 :
        return t('well-done');
      case 1 :
        return t('good-work');
      case 2 :
        return t('excellent');
      default :
        return '';
    }
  }

  function getPlaySendersMessage() {
    return (giftPartCount === 1) ? t('sender-names-message-for-you', { senderName })
         : (props.giftPartIndex === 0) ? t('sender-names-first-message-for-you', { senderName })
         : (props.giftPartIndex === 1) ? t('sender-names-message-for-you', { senderName })
         : (props.giftPartIndex === 2) ? t('sender-names-final-message-for-you', { senderName })
         : '';
  }

  function getOutroAudioFile() {
    return atMuseum
      ? museumGift
        ? assetStore.assets.rOutroAtMuseumMuseumGift
        : assetStore.assets.rOutroAtMuseumPersonalGift
      // not at museum
      : museumGift
        ? assetStore.assets.rOutroNotAtMuseumMuseumGift
        : assetStore.assets.rOutroNotAtMuseumPersonalGift;
  }

  function getOutroAudioTranscript() {
    return atMuseum
      ? museumGift
        ? museum.audio.rOutroAtMuseumMuseumGift.transcript
        : museum.audio.rOutroAtMuseumPersonalGift.transcript
      // not at museum
      : museumGift
       ? museum.audio.rOutroNotAtMuseumMuseumGift.transcript
       : museum.audio.rOutroNotAtMuseumPersonalGift.transcript;
  }

  return (
    <Panel isParent={false}>

      {showFeedbackModal &&
       <FeedbackModal feedbackUrl={museum.feedbackUrl} feedbackText={museum.feedbackText} onFinished={() => {setShowFeedbackModal(false); }} />
      }

      {/* Audio transcriptions live outside of the PanelContent for layout purposes */}
      {section === 'outro' && (
        <AudioTranscription
          giftId={props.gift.id}
          audioReference={`r-part${props.giftPartIndex + 1}-outro`}
          transcript={getOutroAudioTranscript()}
        />
      )}

      <PanelContent>

        {section === 'start' && (
          <>
            <PanelPrompt
              text={getIntroText()}
              background={'transparent-black'}
              onClick={handleContinue}
            />
            {props.recipientLocation === 'at-museum' && <WaitThen wait={4} andThen={handleContinue}/>}
            {props.recipientLocation !== 'at-museum' && <WaitThen wait={3} andThen={handleContinue}/>}
          </>
        )}

        {/* reveal-preview2 - nothing to show */}

        {section === 'show-clue-search' && (
          <PanelPrompt text={giftPart.clue} background={'transparent-black'} />
        )}

        {section === 'need-help' && (
          <PanelPrompt text={getNeedHelpText()} background={'transparent-black'} />
        )}

        {section === 'help-is-here' && (
          <>
            <PanelPrompt
              text={getPreFindText()}
              background={'transparent-black'}
              onClick={gotoFound}
            />
            <WaitThen
              wait={1}
              andThen={gotoFound}
            />
          </>
        )}

        {/* auto continue if at the museum */}
        {section === 'reveal-full' && (props.recipientLocation === 'at-museum') && (
          <WaitThen
            wait={1}
            andThen={gotoFoundAudio}
          />
        )}

        {section === 'play-audio' && (
          <AudioPlayer
            message={getPlaySendersMessage()}
            src={giftPart.note}
            giftId={props.gift.id}
            audioReference={`r-part${props.giftPartIndex + 1}-play-sender-message`}
            onPlaybackComplete={handleAudioPlaybackFinished}
          />
        )}

        {section === 'show-clue-found' && (
          <PanelPrompt text={giftPart.clue} background={'transparent-black'} />
        )}

        {section === 'unwrapped' && (
          <>
            <PanelPrompt
              background={'transparent-black'}
            >
              <DoneIconWrap>
                <SvgIconDone />
              </DoneIconWrap>

              <PanelText>{t('youve-unwrapped-the-whole-gift')}</PanelText>

            </PanelPrompt>

            <WaitThen
              wait={5}
              andThen={handleContinue}
            />
          </>
        )}

        {section === 'outro' && (
          <AudioPlayer
            message={t('ready-for-the-last-bit')}
            src={getOutroAudioFile()}
            giftId={props.gift.id}
            audioReference={`r-part${props.giftPartIndex + 1}-outro`}
            onPlaybackComplete={() => {
              setShowFeedbackModal(true);
              setOutroAudioPlaybackFinished(true);
            }}
          />
        )}

      </PanelContent>

      <PanelButtons>
        {getButtons()}
      </PanelButtons>

    </Panel>
  );
};

export {
  ReceivingPartContent,
};
