import React, { useState } from 'react';

import { t } from '../../../i18n';
import { museum } from '../../../data';
import { assetStore } from '../../../services';

import { Panel, PanelContent } from '../../panel';
import { PanelButtons } from '../../panel-buttons';
import { Button } from '../../buttons';
import { AudioPlayer } from '../../media/audio-player';
import { AudioTranscription } from '../../media/audio-transcription';
import { RecipientLocation } from '../../choose-location';
import { Gift } from '../../../domain';

/**
 * Show the intro content
 */

export interface Props {
  recipientLocation: RecipientLocation;
  audioIntroPlayed: boolean; // todo check this
  gift: Gift;
  onComplete?: () => void;
  handleAudioIntroPlayed: () => void;
}

const ReceivingIntroContent: React.FC<Props> = (props) => {

  // State
  const [audioPlaybackFinished, setAudioPlaybackFinished] = useState(false);

  function handleContinue() {

    // todo: check for skip in global state, show button below
    if (props.onComplete) {
      props.onComplete();
    }
  }

  // Our audio player has finished
  function handleAudioPlaybackFinished() {

    // Update our state
    setAudioPlaybackFinished(true);

    // Props callback
    if (props.handleAudioIntroPlayed) {
      props.handleAudioIntroPlayed();
    }
  }

  // Local
  const atMuseum = (props.recipientLocation === 'at-museum');
  const museumGift = (props.gift.kind === 'MuseumGift');

  // Determine the audio file
  const audioFile = atMuseum
    ? museumGift
      ? assetStore.assets.rIntroContentAtMuseumMuseumGift
      : assetStore.assets.rIntroContentAtMuseumPersonalGift
    // not at museum
    : museumGift
      ? assetStore.assets.rIntroContentNotAtMuseumMuseumGift
      : assetStore.assets.rIntroContentNotAtMuseumPersonalGift;


  // Get the transcript
  function getTranscript() {
    return atMuseum
      ? museumGift
        ? museum.audio.rIntroContentAtMuseumMuseumGift.transcript
        : museum.audio.rIntroContentAtMuseumPersonalGift.transcript
      // not at museum
      : museumGift
        ? museum.audio.rIntroContentNotAtMuseumMuseumGift.transcript
        : museum.audio.rIntroContentNotAtMuseumPersonalGift.transcript;
  }

  return (
    <Panel isParent={false}>

      <AudioTranscription
        giftId={props.gift.id}
        audioReference={'r-intro-start-here'}
        transcript={getTranscript()}
      />

      <PanelContent>

        <AudioPlayer
          message={t('start-here')}
          src={audioFile}
          giftId={props.gift.id}
          audioReference={'r-intro-start-here'}
          onPlaybackComplete={handleAudioPlaybackFinished}
        />

      </PanelContent>

      <PanelButtons>
        {/* todo: reinstate this */}
        {/* {props.audioIntroPlayed && <Button onClick={handleContinue}>Skip</Button>} */}
        {audioPlaybackFinished && <Button onClick={handleContinue} primary={true}>{t('continue')}</Button>}
      </PanelButtons>

    </Panel>
  );
};

export {
  ReceivingIntroContent,
};
