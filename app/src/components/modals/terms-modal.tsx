import React from 'react';
import styled from 'styled-components';

import { t } from '../../i18n';

import { global } from '../../themes/global';
import { ModalDialogOuter } from './base-modal-dialog';
import { Button, Buttons } from '../buttons';
import { TextResize } from '../text-resize';

/**
 * Terms & privacy modal
 */

const Inner = styled.div`
  background-color: white;
  position: absolute;
  bottom: 0;
`;

const Texts = styled.div`
  text-align: center;
  padding: 5% 5% 4%;
  a {
    color: ${global.colour.darkRed};
    opacity: 0.7;
  }
`;

const TopText = styled(TextResize)`
  color: black;
  font-weight: 500;
  margin-bottom: 3%;
  line-height: 1.2;
`;

const MainText = styled(TextResize)`
  color: ${global.colour.lightGreyText};
  margin-bottom: 5%;
  line-height: 1.2;
`;

const TermsButton = styled.button`
  margin-bottom: 2%;
  color: ${global.colour.darkRed};
  opacity: 0.7;
`;

interface Props {
  onAgreeClick: () => void; // Callback when the agree button is clicked
  onShowTerms: () => void; // Callback when the show terms button is clicked
}


const TermsModal: React.FC<Props> = ({ onAgreeClick, onShowTerms }) => {
  return (
    <ModalDialogOuter>
      <Inner>

        <Texts>
          <TopText textSize={40}>{t('this-demo-of-gift-is-hosted-by')}<br/>&nbsp;
            <a href='https://www.blasttheory.co.uk/' target='_blank'>Blast Theory</a>
          </TopText>

          <MainText textSize={40}>
            <a href='https://gifting.digital/gift-experience/' target='_blank'>{t('find-out-more-about-hosting-gift-at-your-museum')}</a>
          </MainText>

          <MainText textSize={35}>{t('short-privacy-statement-text')}</MainText>

          <TermsButton onClick={onShowTerms}>
            <TextResize textSize={40}>{t('read-our-terms-and-privacy')}</TextResize>
          </TermsButton>

        </Texts>

        <Buttons>
          <Button onClick={onAgreeClick} colour='grey'>{t('agree-and-continue')}</Button>
        </Buttons>

      </Inner>
    </ModalDialogOuter>
  );

};

export {
  TermsModal,
};
