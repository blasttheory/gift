import React from 'react';

import { BgSvgFullScreen } from './svg/bg';


const BackgroundSvg: React.FC = () => {
  return <BgSvgFullScreen />;
};

export {
  BackgroundSvg,
};
