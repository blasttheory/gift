import React, { useState } from 'react';
import styled from 'styled-components';

import { t } from '../../i18n';
import { Museum } from '../../domain';

import { Buttons, Button } from '../buttons';
import { ScreenMessage } from './screen-message';
import { ScreenManager } from '../screen-manager';
import { InformationWindow } from '../modals/information-window';
import { HelpContent } from '../information/help';

/**
 * Component to inform user their device in unspported
 * Used for various messages, i.e. Cannot use Chrome to record audio on iOS, or entire device is too old.
 */

interface Props {
  museum: Museum;
  message: string;
}

const DeviceButtons = styled(Buttons)`
  position: absolute;
  bottom: 0;
  left: 0;
  box-sizing: border-box;
`;

const UnsupportedDevice: React.FC<Props> = ({ museum, message }) => {

  // State
  const [deviceListIsOpen, setDeviceListIsOpen] = useState(false);

  return (
    <ScreenManager>
      <ScreenMessage message={message}>
        <DeviceButtons>
          <Button colour='black' onClick={() => { setDeviceListIsOpen(true); }}>{t('show-supported-devices')}</Button>
        </DeviceButtons>
      </ScreenMessage>
      {deviceListIsOpen && (
        <InformationWindow onClose={() => {setDeviceListIsOpen(false); }}>
          <HelpContent markdown={museum.helpMarkdown} />
        </InformationWindow>
      )}
    </ScreenManager>
  );

};

export {
  UnsupportedDevice,
};
