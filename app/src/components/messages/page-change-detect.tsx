import React, { useEffect } from 'react';
import { usePrompt } from 'react-router-dom';

/***
 * Detects when the page reloads/closes/moves away
 * Note: We do not detect or intefere with browser back as would be a bad user experience
 */

interface Props {
  enabled: boolean; // Enable or disable the whole control
  confirmationMessage: string; // Confirmation message
}

const PageChangeDetect: React.FC<Props> = ({
  enabled,
  confirmationMessage,
}) => {
  // Deal with page reloads / navigating off-site
  useEffect(() => {
    if (!enabled) return;

    const onBeforeUnload = (e: BeforeUnloadEvent) => {
      e.cancelBubble = true;
      (e || window.event).returnValue = confirmationMessage; // Gecko + IE
      return confirmationMessage;                            // Webkit, Safari, Chrome
    }

    // Capture and prompt for reload/close
    // Note: Some browsers will ignore the message we give
    window.addEventListener('beforeunload', onBeforeUnload);

    return () => {
      window.removeEventListener('beforeunload', onBeforeUnload);
    }
  }, [enabled, confirmationMessage]);

  // Deal with internal navigation
  usePrompt(confirmationMessage, enabled);

  return null;
};

export {
  PageChangeDetect,
};
