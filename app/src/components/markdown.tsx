import React from 'react';
import { micromark } from 'micromark';

export const Markdown: React.FC<{
  content: string | Buffer;
  allowDangerousHtml?: boolean;
}> = (p) => {
  return (
    <div
      dangerouslySetInnerHTML={{
        __html: micromark(p.content, 'utf8', { allowDangerousHtml: p.allowDangerousHtml ?? false })
      }}
    />
  );
};
