import { v5 as uuidv5 } from 'uuid';
import { Museum } from '../domain';

/**
 * Demo Museum
 *
 * ID: fc0fb3b3-8a60-5727-ae94-f682839ac20c
 */
// tslint:disable max-line-length
export const museum: Museum = {
  // ID should be unique to this particular museum and will be used to identify
  // any gifts created at this museum
  id: uuidv5('https://api.thegift.app/museum/demo', uuidv5.URL),
  slug: 'demo',
  name: '[museum-name]',
  lang: 'en',
  // This should be either an ID of an existing gift on the server, or match a
  // "fake" (already prepared) gift as defined in services/prepared-data.ts
  curatedGiftId: uuidv5('https://api.thegift.app/gift/brighton-museum-1', uuidv5.URL),
  promoLink: '/promo',
  promoDestination: `/gift/${uuidv5('https://api.thegift.app/gift/brighton-museum-1', uuidv5.URL)}`,
  feedbackUrl: 'https://gifting.digital/gift-experience/',
  feedbackText: 'Press continue to learn more about hosting Gift at your museum.',
  homeScreenStartPoint: 'ever-made-a-mixtape',
  homeScreenShowCuratedGift: false,
  termsAndPrivacyMarkdown: require('bundle-text:./terms.md'),
  helpMarkdown: require('bundle-text:./help.md'),
  audio: {
    cStart: {
      url: new URL('../assets/audio/demo/c-start-gift.m4a', import.meta.url).toString(),
      transcript: {
        title: 'Introduction',
        text: `Hey. You can tell a gift that somebody has thought about, compared to one where they panicked, or didn’t bother, or ran out of ideas, can’t you? A good one sort of tells you about what they think of you, what you mean to them.  Today, you’re going make a gift for someone special. They might be next to you right now. They might be on the other side of the world. Close your eyes and try to get a picture of them in your head.`,
      },
    },
    cChoosePart1: {
      url: new URL('../assets/audio/demo/c-choose-part-1.m4a', import.meta.url).toString(),
      transcript: {
        title: 'Choosing part 1',
        text: `Right. It’s time to get out there. Let that person be your filter as you walk – ignore the things you know they’re not going to care about, go and learn more about the objects you know nothing about. Let them know the effort you’ve gone to, to choose the right things. Keep walking as you start to browse slowly. The first thing you choose – why is it right for this person? Is it going to trigger a memory of time you spent together?  Does it shine out in their favourite colour? Or you are you choosing this object for the story it comes with already? The powerful history this thing carries with it – maybe that’s something you want to give?  It’s this person, these objects, and you.  Bring them together...`,
      },
    },
    cChoosePart2: {
      url: new URL('../assets/audio/demo/c-choose-part-2.m4a', import.meta.url).toString(),
      transcript: {
        title: 'Choosing part 2',
        text: `Hey. Nice one. If you know where you’re going next, then just start walking. If you don’t, wander with your eyes wide open. Ok? Part two – the difficult second album – that’s a challenge.  So, where are you going to take them next?`,
      },
    },
    cChoosePart3: {
      url: new URL('../assets/audio/demo/c-choose-part-3.m4a', import.meta.url).toString(),
      transcript: {
        title: 'Choosing part 3',
        text: `Now it’s time to finish this off. Beginning: done. Middle: done. You need an ending – the object this person is going to be left standing in front of when your voice is gone. Does this person know you so well that they’ve already worked out what’s coming last? Push them a little bit. Right? Something they wouldn’t get for themselves. You’ve got a pretty big building full of stories. It’s not too late to surprise them?`,
      },
    },
    cLetThemKnowPart1: {
      url: new URL('../assets/audio/demo/c-let-them-know-part-1.m4a', import.meta.url).toString(),
      transcript: {
        title: 'Record a message for part 1',
        text: `'hey say it’s the thought that counts. Sure, it’s a cliché but it’s true, right? I want to ask you to speak to this person. Let them hear your voice.  Talk to them as if you were leaving a voicemail.  It’s ok to be on your phone in here as long as you keep it low-key. You’re not disturbing anyone. You’ve got a moment to record a note about this object. What was the thought, and why does it count? Imagine them standing here after they’ve just found it – what do they need to hear? I can’t help you on this one – sorry – it’s better if it comes from you.   Take your time to think, and tap the button when you’re ready.`,
      },
    },
    cLetThemKnowPart2: {
      url: new URL('../assets/audio/demo/c-let-them-know-part-2.m4a', import.meta.url).toString(),
      transcript: {
        title: 'Record a message for part 2',
        text: `'ime for your second message, to go along with your photo. I’m wondering how far you’ve walked, and whether those steps have taken you across cultures, continents, empires? When you’re ready, hit record.`,
      },
    },
    cLetThemKnowPart3: {
      url: new URL('../assets/audio/demo/c-let-them-know-part-3.m4a', import.meta.url).toString(),
      transcript: {
        title: 'Record a message for part 3',
        text: `It’s time to record your final message. Someone gave me this advice recently, and while we’re all being so generous, I thought... why not pass it on. They said to me – don’t worry about explaining too much. That’s it. I didn’t say it was long. I just thought, you know, it could help... So, is there a thought you want to leave them with?`,
      },
    },
    cShare: {
      url: new URL('../assets/audio/demo/c-share.m4a', import.meta.url).toString(),
      transcript: {
        title: 'Postscript',
        text: `Hey. So, there are people who believe that the more you give away, the richer you are.  Are you one of them?  I don’t think it just has to be about ridding yourself of all your material wealth – like a monk – I think it can just be about working out what you have to give and whether you’re willing to give it. You’ve given these things without taking them. You’ve taken time, and I know someone is going to appreciate that. Walking in your footsteps, they’ll keep you in mind, and know that you did it for them.  Yeh. I think they’re gonna like it.`,
      },
    },
    rIntroContentAtMuseumMuseumGift: {
      url: new URL('../assets/audio/demo/r-intro-content-local-museum.m4a', import.meta.url).toString(),
      transcript: {
        title: 'Introduction',
        text: `Hey. So you’ve come to the museum. This gift has been made... for you. I’ll explain. Someone here wanted to share the objects in this gift with you and recorded messages for each one. Walk around and see if you can find the first one. If you get stuck, show the photo to a member of staff. Someone’s gonna know where it is. I’ll see you on the other side.`,
      },
    },
    rIntroContentAtMuseumPersonalGift: {
      url: new URL('../assets/audio/demo/r-intro-content-local-personal.m4a', import.meta.url).toString(),
      transcript: {
        title: 'Introduction',
        text: `Hey. So you’re the one getting the gift. Lucky. I know someone has been thinking about you. If you want to just start walking, slowly, in any direction, I’ll explain. Ok? Someone you know has visited the museum. You were the lens for everything they looked at. They searched out things they thought you would care about and they took photos and made messages. When you open a part, walk around and see if you can find it. If you get stuck, show the photo to a member of staff. Someone’s gonna know where it is. I’ll see you on the other side.`,
      },
    },
    rIntroContentNotAtMuseumMuseumGift: {
      url: new URL('../assets/audio/demo/r-intro-content-remote-museum.m4a', import.meta.url).toString(),
      transcript: {
        title: 'Introduction',
        text: `Hey. So... this gift has been made for you. I’ll explain. Someone at the museum wanted to share the objects in this gift with you and recorded messages for each one. Tap continue and I’ll see you on the other side.`,
      },
    },
    rIntroContentNotAtMuseumPersonalGift: {
      url: new URL('../assets/audio/demo/r-intro-content-remote-personal.m4a', import.meta.url).toString(),
      transcript: {
        title: 'Introduction',
        text: `Hey. So you’re the one getting the gift. Lucky.  I know someone has been thinking about you. Um...let me explain. Someone you know has visited the museum. You were the lens for everything they looked at. They searched out things they thought you would care about and they took photos and made messages. This is kind of between you two, and the objects they chose, so I’m not going to say much more. I’ll see you on the other side.`,
      },
    },
    rOutroAtMuseumMuseumGift: {
      url: new URL('../assets/audio/demo/r-outro-local-museum.m4a', import.meta.url).toString(),
      transcript: {
        title: 'Postscript',
        text: `Hey. Well here we are – on the other side. I wonder...did the gift surprise you, or did you know these objects already?... Objects in a museum,...not owned, but given. Now you've had this gift from the museum, maybe you'll make a gift for someone you care about?`,
      },
    },
    rOutroAtMuseumPersonalGift: {
      url: new URL('../assets/audio/demo/r-outro-local-personal.m4a', import.meta.url).toString(),
      transcript: {
        title: 'Postscript',
        text: `Hey. Well here we are – on the other side. I figured I’d leave you to it.  I wonder...did the gift surprise you, or do you know each other so well that surprises are kind of impossible... Objects in a museum,...not owned, but given. Maybe one day you’ll return the favour?`,
      },
    },
    rOutroNotAtMuseumMuseumGift: {
      url: new URL('../assets/audio/demo/r-outro-remote-museum.m4a', import.meta.url).toString(),
      transcript: {
        title: 'Postscript',
        text: `Hey. Well here we are – on the other side. I wonder...did the gift surprise you, or did you know these objects already?... Objects in a museum,...not owned, but given. Now you've had this gift from the museum, may be you'll visit and make a gift for someone you care about?`,
      },
    },
    rOutroNotAtMuseumPersonalGift: {
      url: new URL('../assets/audio/demo/r-outro-remote-personal.m4a', import.meta.url).toString(),
      transcript: {
        title: 'Postscript',
        text: `Hey. Well here we are – on the other side. I figured I’d leave you to it. I wonder...did the gift surprise you, or do you know each other so well that surprises are kind of impossible? If getting this gift on your phone really gave you a taste for it, you could make a trip to the museum and see how it feels to stand with these objects, like this person was when they were thinking of you.`,
      },
    },
  },
};
