# Terms & Privacy

_Last updated: July 11, 2019_

Welcome to Gift. This document sets out the terms for using the Gift website and how we handle your data. Please read this document and if you have any questions then please do get in touch.

Here are the headlines:

### 1. Play nice

Please refrain from submitting abusive, offensive, or otherwise inappropriate content.

### 2. Follow the rules of the museum

Please follow guidelines given by the museum. Be aware that rules for photography may vary for specific exhibitions and be sensitive to other visitors when using your phone.

### 3. Be aware of what you share

We will never share personal information with third parties without your consent.

However, gifts that you create can be shared publicly by those that you send them to. Your contributions may also be used - in an anonymised form - to document the project and for research.

________________________________________________________________________________

## Terms of Use

This document forms the Terms of Use (the 'Agreement') for the services provided by Blast Theory ('we', 'our') as part of the Gift website (the 'Website'). By accessing or using the Website in any way, users ('User', 'Users', 'you', 'your') agree to abide by this Agreement.

Please refer to our [Privacy Policy](#privacy-policy) below for information on how we collect, use and disclose personally identifiable information from our users. By using the Website you agree to our use, collection and disclosure of personally identifiable information in accordance with the Privacy Policy.

### 1. Acceptance and changes to these terms

The Website is for personal and non-commercial use only, and your use of the Website, is conditioned on and subject to your compliance with this Agreement. If you do not agree with this Agreement, you should leave the Website and discontinue its use . Please read this Agreement carefully. If you wish to become a Member, you must read this Agreement and indicate your acceptance.

We may revise this Agreement at any time, and we will notify you of any changes by posting notice in the Website. If you do not agree with the changes, you must terminate your use of the Website.

If we make any material change to this Agreement we may also notify you via the email address you provide to us on registration, or as otherwise required by law and ask that you consent to those material changes. Please note that it is your responsibility to keep your registration information with us, including your email address for notification purposes, up-to-date.

Any changes to the Agreement that amend the licenses provided in this Agreement shall amend any licenses granted before the changes take effect, as well as any granted after the changes takes effect. These terms shall always bear a 'last updated' date at the top of the page showing the date on which they were last updated.

### 2. User conduct

You are solely responsible for your conduct on the Website, and for any content, including but not limited to text, audio and photos (collectively, 'User Content'), that you contribute in the Website.

You may not use the Website to abuse, harass, threaten, impersonate or intimidate others or to create or submit unwanted email or other messages.

You may not use the Website for any illegal or unauthorized purpose. International Users agree to comply with all local laws regarding conduct and acceptable content.

You may not collect or harvest any personally identifiable information from the Website.

You may not use the Website to upload, transmit or link to Prohibited Content. Generally, Prohibited Content includes content or other material that we believe:

* is abusive, deceptive, pornographic, obscene, defamatory, slanderous, offensive, encourages conduct that would violate or violates any law, harassing, hateful, racially or ethnically offensive, or otherwise inappropriate;

* comprises material that is copyrighted or protected by trade secret or otherwise subject to third party proprietary rights, including privacy and publicity rights, unless express written permission of the owner is obtained to use the material and to grant all of the license rights granted herein;

* violates or otherwise encroaches on the rights of others;

* advocates illegal activity;

* harms anyone, including minors; or

* provides a link to any of the above.

You are responsible for any activity that occurs through your use of the Website.

You are responsible for maintaining the confidentiality of any personal or personally identifiable data you upload and are responsible for complying with all applicable laws governing collection, storage, processing, use and transfer of such information.

Blast Theory has the sole discretion to determine whether Content is Prohibited Content, and any Content submitted to the Website may be subject to examination and may be deleted, moved or edited without notice.

Any breach or non-compliance of any of these user conduct terms will entitle us to immediately and without notice (i) terminate any Member account, which could result in the removal of any data in the account from our servers and any User Content (including permanent deletion of data), and (ii) block you from any further use of the Website.

### 3. User generated content

The Website allows Users to create gifts by generating content, comprising photos, text and audio during their visit to a museum, and to share these gifts. We do not claim any ownership interest in your User Content, but we do need the right to use your User Content.

In order for us to make the User Content you contribute available in the Website, and to operate, market and promote the Website, we need the right to make use of such User Content in accordance with and subject to this Agreement.

For example, if you take photos or record audio, we need your license to display that User Content in the Website, so that recipients can view that User Content.

We may also display User Content for the purposes of documentation and research undertaken by the Project, we therefore need your license to 'modify, adapt, translate, and create derivative works from' your content.

Therefore, by contributing User Content to the Website or creating it in the Website:

* You automatically grant to us an irrevocable and perpetual (except as set forth in this Agreement), non-exclusive, transferable, fully-paid, royalty-free (except as expressly set forth in this Agreement), worldwide license, by ourselves or with others, to use, copy, distribute, publicly perform, publicly display, print, publish, republish, excerpt (in whole or in part), reformat, translate, modify, revise and incorporate into other works, that User Content and any works derived from that User Content, in any form of media or expression, in the manner in which the Website from time to time permits User Content to be used, in compliance with all applicable laws, and to license or permit others to do so. Our rights under this license remain in place even after termination of your Membership and account.

* You represent and warrant to Blast Theory that (a) you are the sole owner, author and copyright owner of User Content you contribute to the Website or you have permission from the author, owner or copyright owner to make such User Content available to the Website as 'User Content', and (b) such User Content does not infringe upon any third party rights (including but not limited to any copyright, patent, trademark, trade secret, other intellectual property rights, moral rights, privacy rights or publicity rights); (c) such User Content complies with this Agreement and does not contain any defamatory, libelous or obscene material; (d) such User Content does not violate any applicable laws, regulations, or contain any false statements or misrepresentations; and (e) you have the consent, release, and/or permission of each and every identifiable individual person, or the owner/author of any objects or work in such User Content to use the name or likeness of each and every such identifiable individual person, object or work, to enable inclusion and use of such User Content in the manner contemplated by the Website and this Agreement.

### 4. Your privacy responsibilities

You represent and warrant to Blast Theory that you have obtained all required consents with respect to any personal information (of any other person) provided by you to Blast Theory, and that - subject to our Privacy Policy - Blast Theory may use such personal information for the purposes of operating the Website.

You agree to comply with all applicable data privacy laws.

### 5. Respecting the intellectual property of others

Blast Theory respects intellectual property rights. If you believe that any User Content in the Website infringes on another's intellectual property rights you may contact us. Blast Theory reserves the right to remove User Content without prior notice. Blast Theory may also terminate a Users’ access to the Website, immediately and without notice, if Blast Theory believes, in its sole discretion, that the User is infringing any third party rights (including but not limited to any copyright, patent, trademark, trade secret, other intellectual property rights, moral rights, privacy rights or publicity rights).

### 6. Indemnity

You agree to indemnify and hold harmless the Blast Theory Parties from any damage, liability, cost, expense, loss, claim or demand (collectively, 'Losses') including reasonable lawyers’ fees, due to or arising out of your use of the Website, including your breach of this Agreement, and including also any claims that Content provided by you to us or Members infringes the intellectual property or other rights of any third party.

### 7. Disclaimers

Blast Theory is not responsible or liable in any manner for any User Content posted. Although we provide rules for user conduct, we do not control and are not responsible for what Users do, post, transmit or share in the Website.

Under no circumstances will Blast Theory be responsible for any loss or damage, including any loss or damage to any User Content, or for personal injury or death, resulting from anyone's use of the Website, any Content posted on or through the Website, or any interactions between users of the Website, whether online or offline. You post Content in the Website at your own risk.

Blast Theory expressly disclaims all warranties of any kind, whether express or implied, including such warranties as may be applicable under Sale of Goods legislation or other statutes, and including but not limited to the implied warranties of merchantability, fitness for a particular purpose, performance and non-infringement, or of availability or results. Blast Theory does not warrant that (i) the Website will meet your specific requirements, (ii) the Website will be uninterrupted, timely, secure, or error-free, (iii) the results that may be obtained from the use of the Website will be accurate or reliable, (iv) the quality of any products, services, information, or other material obtained by you through the Website will meet your expectations, and (v) any errors in the Website or any software will be corrected.

### 8. Limitation of liability

In no event will the Blast Theory be liable to you for any indirect, consequential, exemplary, incidental, special, aggravated, exemplary or punitive damages, or damages for loss of profits, goodwill, use, data or other intangible losses, whether arising out of contract, tort, negligence (including strict liability), warranty, indemnity or otherwise, arising from your use of the Website, or any of the Website Content or other materials on, accessed through or downloaded from the Website, even if the Blast Theory Parties are aware or have been advised of the possibility of such damages.

### 9. Electronic communications

The communications between you and Blast Theory use electronic means, whether you use the Website or send Blast Theory e-mails, or whether Blast Theory posts notices on or through the Website or communicates with you via e-mail. For contractual purposes, you (a) consent to receive communications from Blast Theory in an electronic form; and (b) agree that all terms and conditions, agreements, notices, disclosures, and other communications that Blast Theory provides to you electronically satisfy any legal requirement that such communications would satisfy if it were be in writing. The foregoing does not affect your statutory rights.

### 10. Termination

This Agreement continues until terminated by either Blast Theory or you. Blast Theory reserves the right to refuse access to the Website to any person at any time for any reason, or for no reason, in its sole discretion.

Blast Theory may terminate this Agreement and your account, delete your account and remove any Content or information that you have posted in the Website or and/or prohibit you from using or accessing the Website (or any portion, aspect or feature of the Website) for any reason, or no reason, at any time in its sole discretion, with or without notice. If we terminate your account or this Agreement, our license to your User Content shall continue as described in this Agreement.

If you terminate this Agreement, the effective time of termination is a reasonable period of time, not less than 30 days, after we receive your notice. After termination of your license to us with respect to all of the User Content you contribute to the Website or termination of your account, you will no longer have access to your account and all information contained therein may be deleted.

________________________________________________________________________________

## <a name="privacy-policy"></a> Privacy Policy

This Privacy Policy governs the manner in which Blast Theory ('us', 'our', 'we') collects, uses, maintains and discloses information collected from users (each, a 'User') of the GIFT Website ('Website'). This privacy policy ('Policy') applies to the Website and all related products and services offered by Blast Theory as part of the GIFT project ('Project').

### 1. Your Data

#### Personal identification information

We may collect personal identification information ('Personal Information') such as names and email addresses from Users in a variety of ways, including, when Users register on the Website, respond to a survey, fill in a form, and in connection with other activities, services or features we make available on our Website.

Where User Content discloses full names, addresses or other personal identification information that User Content is treated as Personal Information.

#### Non-personal information

We may collect non-personal information ('Usage Data') about Users whenever they interact with our Website. Usage Data may include the type of device, the time a User accesses the Website, technical information about the Website's use.

Where User Content is deemed not to disclose Personal Information, for the purposes of this policy, it is treated as non-personal Usage Data.

### 2. How we use your data

We may collect and use Personal Information for the following purposes:

* To provide core Website functionality: we use User information to deliver core features and functionality in the Website. For example, User Content is made available via a publicly accessible URL to share gifts with recipients.

* To personalise the User experience: we may use User information to understand how Users use the services and resources provided in our Website and to customise User experience of our products and services. For example, email addresses may be used to associate gifts created by Users to a Member account.

* To send periodic emails: we may use the email address to send Users important information and updates about the Website. It may also be used to respond to their inquiries, questions, and/or other requests. If the User decides to opt-in to our mailing list, they will receive emails that may include company news, updates, related product or service information, etc. If at any time the User would like to unsubscribe from receiving future emails, we include unsubscribe instructions at the bottom of each email.

We may collect and use Usage Data for the following purposes:

* To improve our Website: we may use information in the aggregate to understand how Users as a group use the services and resources provided on our Website.

* To present outcomes from the Project: we may use non-personal information from Users to publicly present outcomes from the Project, including individual audio and text responses to questions and aggregated answers to questions within the Website.

* To do research: we may use anonymised data for researching User journeys and interactions. We may share this information with Website research partners, funding partners and at future conferences and presentations on an ongoing basis.

### 3. How we protect your data

We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorised access, alteration, disclosure or destruction of User personal information and data stored. All data exchanged between the Website and our servers happens over an industry-standard TLS secured communication channel and is encrypted and protected with digital signatures. All data is encrypted at rest using strong encryption.

### 4. Sharing and access

We do not sell, trade, or rent Users Personal Information to others. We may share generic aggregated information not linked to any Personal Information regarding individual Users with our research partners, funding partners as part of presentations and to other Website Users.

Users can request their data or request that their data is deleted at anytime by emailing Blast Theory at the address below.

### 5. Deletion of Personal Information

Personal Information will be stored as long as the services provided by the Website are available and will be deleted no more than 6 months after these services are terminated.

### 6. Your acceptance of these terms

By using the Website, you signify your acceptance of this Policy. If you do not agree to this Policy, please do not use our Website. Your continued use of the Website following the posting of changes to this Policy will be deemed your acceptance of those changes.

### 7. Changes to this Policy

We have the discretion to update this Policy at any time and we will notify you of any changes by posting notice in the Website. Your use of the Website after notice of any changes to the Policy is your agreement to those change. If you do not agree with the changes, you must terminate your use of the Website.

If we make any material change to this Policy we may also notify you via the email address you provide to us on registration, or as otherwise required by law and ask that you consent to those material changes. Please note that it is your responsibility to keep your registration information with us, including your email address for notification purposes, up-to-date.

________________________________________________________________________________

## Contacting us

If you have any questions about this Privacy Policy, the practices of the Website, or your dealings with the Website, please contact us at:

Blast Theory\
Unit 5, 20 Wellington Road\
Portslade, Brighton\
East Sussex, BN41 1DN\
UK

Email: [gift@blasttheory.co.uk](mailto:gift@blasttheory.co.uk)
