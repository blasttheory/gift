# Help

##### What is Gift?

Gift invites you to pick someone you love and create the perfect digital gift for them on your phone. Choose beautiful objects, record a personal message for each one, then send it.

##### Who can make a gift?

You can. Just bring your smart phone to the museum to begin.

##### Who can I make gifts for?

You can choose anyone. Someone you wish could be with you at the museum. Or someone who’s standing next to you now. It’s up to you.

___

## About phones and devices

##### What do I need to experience Gift?

To take part in the museum, you will need a smart phone or tablet.

##### What devices can I use?

Gift is designed to run on most modern smart phones. It has been tested on devices running Android 6.0 or newer and in Safari on iOS11 or newer.

You can also open gifts (but not create gifts) on a laptop or desktop computer.

##### What devices and browsers are not supported?

Chrome on iOS is not currently supported. Older budget Android devices may also have problems loading Gift.

##### Do I need to install an app?

No. Gift runs entirely in your phone’s web browser so there’s no need to download an app.

##### Do I need to use headphones?

No. You should feel free to use your phone speaker to listen to Gift while in the museum. Some wireless headphones are not supported.

##### Why can't I record messages?

Some headsets and bluetooth headphones are incompatible with voice recording from your phone browser. If you're using a headset or headphones, try disconnecting them to record your messages.

If recording fails to start at all, check the microphone permissions for your browser in your phone's settings.

##### Do I need to use Wifi?

You will need an internet connection on your phone to use Gift. This can use either WiFi or mobile data.

___

## About making gifts

##### Who can do this?

Visitors aged as young as 8 and over 75 can create gifts. Those aged 10 or under may need assistance from someone they’re with.

##### Can I make a gift as a group or with someone?

Yes. You can work with other people to make a gift on a single phone.

##### Can I do this while wandering around doing other things?

Yes, it is possible and works well like this.

##### What’s involved in making a gift?

You explore the museum with a person you love or who is special in mind. An audio narrator guides you in choosing up to three objects from the collection. They must take a photo of each object, record an audio message about why you chose it and enter a written clue to explain how to find the object in the museum.

##### Who can I make gifts for?

You can choose anyone. Someone you wish could be with you at the museum today. Or someone who’s right next to you now.

##### Do I have to choose three objects?

No. You can create gifts with one, two or three parts.

##### How long does it take to make a gift?

It’s up to you. It will take between 2 and 20 minutes.

##### How do I send my gifts?

To send gifts, you’ll need the email or mobile number of the person you’ve chosen or have them as a contact in WhatsApp or Facebook Messenger.

##### Can I make gifts when not at a museum?

No, the experience is designed for creating gifts at the museum only.

##### Can I open gifts I’ve made myself?

Yes. You can view gifts you’ve sent by opening the link in your sent messages.

___

## About receiving gifts

##### Can someone without a smart phone open gifts?

Yes, recipients can open your gifts on a desktop computer, laptop or tablet.

##### Do recipients need to come to the museum?

No, recipients are encouraged to open the gifts they receive at the museum, but you can open them anywhere if they choose to.

##### I’ve lost the gift someone sent me. How can I find it?

If you’ve lost the link for your gift please ask the sender to resend it. Gifts are sent as public links.

___

## About privacy and data

##### Are the gifts that I make private?

No, gifts are shared using public links and can be opened by anyone that the creators give the link to. Please note that these links can be passed on or shared publicly.

##### Who has my data? Is it shared with anyone?

Data for gifts is stored by Blast Theory. To read our terms of use & privacy policy choose ‘Privacy’ in the main menu.

Blast Theory will never share your personal information with third parties without your consent. However, in order to share gifts, users give Blast Theory a license to publish contributions; including photos and audio messages on the user’s behalf. Under this license, these contributions may also be used - in an anonymised form - to document the project and to study how the website is used.

Blast Theory also gathers anonymised usage data from use of the website to help improve its service.

##### Why does Gift need access to my camera and microphone?

You take photos and record voice messages to create gifts. Your browser needs permission to access the camera and microphone on your device to do this.

##### How much data do gifts use?

Gifts are generally less than 10MB each.

##### How long do you store my gifts for? Will you delete them?

Gift is planned to run at Brighton Museum until October, 2019. Gifts created by visitors will only be accessible during this time. Gifts may also be deleted at the request of users (see below). Please read our terms and privacy for details of storing and deleting your data.

##### How can I report abuse or request to delete a gift or my data?

Please email Blast Theory at gift@blasttheory.co.uk or choose 'Feedback' from the main menu.

___

## Accessibility and more

##### Does Gift have sound?

Yes. Gifts you receive include audio messages from the sender. The website also has a narrator that guides you through creating and opening gifts.

##### I have a hearing impairment. Can I still use Gift?

Gifts contain audio messages that you will need to listen to when they open gifts. When creating gifts you need to record audio messages to accompany each object using your phone. Transcripts of the narrator’s audio are provided throughout.

##### I have a vision impairment. Can I still use Gift?

Photos are used in the interface to identify the objects in gifts. These are shown alongside text clues to help locate the objects. You are advised in the interface to ask a member of staff if you cannot find an object in your gift. When creating gifts, you are also asked to take photos of the objects you would like to choose.

##### I have problems using Gift. How can I tell you about them?

Please let us know about any experiences or issues using Gift choose 'Feedback' from the main menu. We'll do our best to fix them.

___

## Who made the work and why?

##### Who made Gift?

Gift is made by the artist group, Blast Theory as part of a three year research project funded by the European Union’s Horizon 2020 research and innovation programme under grant agreement No 727040. It is a collaboration with IT University Copenhagen, University of Nottingham, University of Uppsala, Blast Theory, Next Game, the Europeana Foundation and Culture24.

Visit the project website for more information: <a href='https://gifting.digital' target='_blank'>gifting.digital</a>

##### Who are Blast Theory?

Visit our website: <a href='https://www.blasttheory.co.uk/projects/gift' target='_blank'>www.blasttheory.co.uk</a>

##### Why is Gift at Brighton Museum?

Gift gives you a new pathway through museums, one that takes you off the beaten track onto a pathway of your own. It is a new way of experiencing museums through the eyes of someone special to you.

##### How do I feedback on my experience of Gift?

To let us know about your experience,  choose 'Feedback' from the main menu.

___

## Contacting

If you have any questions or need further help please contact us at:

Blast Theory\
Unit 5, 20 Wellington Road\
Portslade, Brighton\
East Sussex, BN41 1DN\
UK

Email: [gift@blasttheory.co.uk](mailto:gift@blasttheory.co.uk)
