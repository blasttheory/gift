/**
 * The config module.
 *
 * This module is responsible for obtaining and parsing any necessary
 * configuration.  It is the responsibility of this module to fail
 * hard and fast if any required config is missing.  Else the bugs
 * will probably just appear in production when you least expect it.
 * That wouldn't be good.
 */

class ConfigError extends Error {
  public name = 'ConfigError';
}

// =====================================================================

export interface Config {
  environment: string;
  apiUri: string;
  museumOverride?: string;
}


export const config: Config = {
  environment: readAsString(process.env.ENVIRONMENT),
  apiUri: readAsString(process.env.API_URI),
  museumOverride: process.env.MUSEUM_OVERRIDE,
};


// =====================================================================


// =======
// Helpers
// =======

function readAsString(val?: string): string {
  return readOrThrow(val);
}

function readOrThrow(val?: string): string {
  if (val === undefined || val.trim() === '') {
    throw new ConfigError(`Required value missing`);
  }

  return val.trim();
}
