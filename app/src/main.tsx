import React, { useEffect } from 'react';
import { BrowserRouter, Navigate, Route, Routes, useLocation } from 'react-router-dom';

import { useAsync } from './utils/use-async';

import { i18nSetup, setLocale, t } from './i18n';
import { museum } from './data';
import { assetStore, events } from './services';
import { appStartedEvent, locationChangedEvent } from './event-definitions';

import { NotFound } from './screens/not-found';
import { ReceiveGiftScreen } from './screens/receive-gift';
import { CreateGiftScreen } from './screens/create-gift';
import { HomeScreen } from './screens/home';
import { LandscapeMessage } from './components/messages/landscape-message';
import { WorkingProgress } from './components/messages/working-progress';
import { ErrorMessage } from './components/messages/error-message';

i18nSetup();
setLocale(museum.lang);

export const Main: React.FC = () => {
  useEffect(() => events.track(appStartedEvent()), []);

  return (
    <BrowserRouter>
      <LandscapeMessage />
      <MainInner />
    </BrowserRouter>
  );
};


const MainInner: React.FC = () => {
  const location = useLocation();
  useEffect(() => {
    events.track(locationChangedEvent(location.pathname, location.search, location.hash));
  }, [location]);

  const [assetPreload] = useAsync(() => assetStore.preload(), []);

  if (assetPreload.kind === 'running') { return (
    <WorkingProgress text={t('gift-is-loading')} />
  ); }

  if (assetPreload.kind === 'failure') { return (
    <ErrorMessage message={t('couldnt-load-gift-assets')} />
  ); }


  return (
    <Routes>

      <Route
        path='/'
        element={<HomeScreen />}
      />

      <Route
        path='/create-gift'
        element={<CreateGiftScreen />}
      />

      {/* "Promo" link: direct access to museum gift, root, or other path */}
      <Route
        path={`${museum.promoLink}`}
        element={<Navigate replace={true} to={museum.promoDestination} />}
      />

      <Route
        path='/gift/:giftId'
        element={<ReceiveGiftScreen />}
      />

      <Route
        path='*'
        element={<NotFound />}
      />

    </Routes>
  );
}
