/**
 * Domain
 *
 * This file defines the app's view of the domain. This is considered distinct
 * from the platform/api and should not be directly coupled.
 */

export type Id = string;
export type MuseumId = Id;
export type GiftId = Id;
export type PhotoUrl = string;
export type AudioRecordingUrl = string;

export type Locale = 'en' | 'cmn';

export interface LocalFile {
  url: string;
  mimeType: string;
}

export interface GiftPart {
  photo: PhotoUrl;
  note: AudioRecordingUrl;
  clue: string;
}

export interface Gift {
  id: GiftId;
  kind: 'MuseumGift' | 'PersonalGift';
  museumId: MuseumId;
  senderName: string;
  recipientName: string;
  parts: GiftPart[];
}


export interface InProgressGiftPart {
  photo: LocalFile;
  note: LocalFile;
  clue: string;
}

export interface InProgressGift {
  id: GiftId;
  museumId: MuseumId;
  senderName?: string;
  recipientName?: string;
  parts: InProgressGiftPart[];
}

export interface AudioAsset {
  url: string;
  transcript: {
    title: string;
    text: string;
  };
}

export interface Museum {
  id: MuseumId;
  slug: string;
  name: string;
  lang: Locale;
  curatedGiftId: GiftId | null;
  promoLink: string;
  promoDestination: string;
  feedbackUrl: string;
  feedbackText: string;
  homeScreenStartPoint: 'ever-made-a-mixtape' | 'new-gift';
  homeScreenShowCuratedGift: boolean;
  termsAndPrivacyMarkdown: string;
  helpMarkdown: string;
  audio: {
    cStart: AudioAsset,
    cChoosePart1: AudioAsset,
    cChoosePart2: AudioAsset,
    cChoosePart3: AudioAsset,
    cLetThemKnowPart1: AudioAsset,
    cLetThemKnowPart2: AudioAsset,
    cLetThemKnowPart3: AudioAsset,
    cShare: AudioAsset,
    rIntroContentAtMuseumMuseumGift: AudioAsset,
    rIntroContentAtMuseumPersonalGift: AudioAsset,
    rIntroContentNotAtMuseumMuseumGift: AudioAsset,
    rIntroContentNotAtMuseumPersonalGift: AudioAsset,
    rOutroAtMuseumMuseumGift: AudioAsset,
    rOutroAtMuseumPersonalGift: AudioAsset,
    rOutroNotAtMuseumMuseumGift: AudioAsset,
    rOutroNotAtMuseumPersonalGift: AudioAsset,
  };
}
