import { promisify } from 'util';
import * as fs from 'fs';
import { v4 as uuidv4 } from 'uuid';

import {
  CreateGiftRequest,
  createGiftRequestSchema,
  CreateGiftResponse,
  GetGiftResponse,
} from '../../common/api-schema';
import { Lib } from '../../lib';
import { TranscodeResult } from '../../lib/services/transcode';
import { Logger } from '../../util-libs/logging';
import { assertNever } from '../../util-libs/prelude';
import { checkUrl, checkBody } from '../../util-libs/validatation';
import { ApiRouter } from './router';

export const router = new ApiRouter();


router.post('/gift', async (ctx) => {
  const body = await checkBody<CreateGiftRequest>(ctx, createGiftRequestSchema);
  const giftId = body.id;

  // Transcode the assets...
  const assetReplacedParts: { photo: string, note: string, clue: string }[] = [];

  for (const part of body.parts) {
    const photoAsset = await processAsset('image', giftId, part.photo, ctx.lib, ctx.logger);
    const noteAsset = await processAsset('audio', giftId, part.note, ctx.lib, ctx.logger);

    ctx.logger.debug('Done', {
      photo: photoAsset,
      note: noteAsset,
    });

    assetReplacedParts.push({
      photo: photoAsset,
      note: noteAsset,
      clue: part.clue,
    });
  }

  const result = await ctx.lib.gift.create({
    id: body.id,
    kind: 'PersonalGift',
    museumId: body.museumId,
    senderName: body.senderName,
    recipientName: body.recipientName,
    parts: assetReplacedParts,
  });

  if (result.kind === 'Success') {
    ctx.status = 201;
    (ctx as { body: CreateGiftResponse }).body = result.data;
    return;
  }

  if (result.kind === 'IdAlreadyExists') {
    ctx.throw(409, { error: 'That gift ID already exists' });
    return;
  }

  assertNever(result);
});


router.get('/gift/:giftId', async (ctx) => {
  const { giftId } = checkUrl(ctx, 'giftId');

  const gift = await ctx.lib.gift.findById(giftId);

  if (!gift) {
    ctx.throw(404);
    return;
  }

  (ctx as { body: GetGiftResponse }).body = gift;
});


/**
 * Helper to retrieve a user-upload, process (transcode) the media, and upload
 * to the asset storage.
 *
 * TODO: Make this part of the lib.
 *
 * @returns The URL of the final uploaded asset.
 */
async function processAsset(
  kind: 'image' | 'audio',
  giftId: string,
  userUploadPath: string,
  lib: Lib,
  logger: Logger,
): Promise<string> {
  const userUploadLocalPath = await lib.storage.getUserUpload(userUploadPath);

  let transcodeResult: TranscodeResult;
  try {
    transcodeResult
      = (kind === 'image') ? await lib.transcode.transcodeImage(userUploadLocalPath)
      : (kind === 'audio') ? await lib.transcode.transcodeAudio(userUploadLocalPath)
      : assertNever(kind);
  } finally {
    await promisify(fs.unlink)(userUploadLocalPath).catch((unlinkError) => {
      logger.error(unlinkError, 'CleanupError');
    });
  }

  let assetRemoteUrl: string;
  try {
    assetRemoteUrl = await lib.storage.uploadAsset(
      `${giftId}/${uuidv4()}.${transcodeResult.extension}`,
      fs.createReadStream(transcodeResult.path),
    );
  } finally {
    await promisify(fs.unlink)(transcodeResult.path).catch((unlinkError) => {
      logger.error(unlinkError, 'CleanupError');
    });
  }

  return assetRemoteUrl;
}
